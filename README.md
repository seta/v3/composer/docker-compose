# docker-compose


## Usage

### Production

Create `.env` environment file for docker compose as in `.env.example`

Run:
```
docker compose up
```

### Development

For development, you'll have to run the `docker-composer-dev.yml` defined in each project and stop the respective container from `seta-compose` namespace.

### Test

Create `.env.test` environment file for docker compose as in `.env.example`

Run:
```
docker compose -f docker-compose-test.yml --env-file .env.test up
```
