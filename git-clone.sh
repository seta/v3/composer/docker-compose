#!/bin/bash

cd ..

git clone  git@code.europa.eu:seta/v3/backend/admin-ws.git
git clone  git@code.europa.eu:seta/v3/backend/data-app.git
git clone git@code.europa.eu:seta/v3/backend/data-repos.git
git clone git@code.europa.eu:seta/v3/backend/nlp-ws.git
git clone git@code.europa.eu:seta/v3/backend/rest-ws.git
git clone git@code.europa.eu:seta/v3/backend/search-ws.git
git clone git@code.europa.eu:seta/v3/frontend/docs-app.git
git clone git@code.europa.eu:seta/v3/frontend/proxy-ws.git
git clone git@code.europa.eu:seta/v3/frontend/web-app.git
git clone git@code.europa.eu:seta/v3/harvest/harvest-cellar.git
git clone git@code.europa.eu:seta/v3/backend/search-fast-ws.git
